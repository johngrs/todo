#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QFile>
#include <QInputDialog>
#include <QSettings>
#include <QTextStream>

#define FILENAME "todo.lst"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mTasks()
{
    ui->setupUi(this);
    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);
    readSettings();
    updateStatus();
}

MainWindow::~MainWindow()
{
    delete ui;
    ui=nullptr;
}

void MainWindow::addTask()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Add task"),
                                         tr("Task name"), QLineEdit::Normal,
                                         tr("Untitled task"), &ok);
    if (ok && !name.isEmpty()) {
        Task* task = new Task(name);
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
    }
}

void MainWindow::removeTask(Task* task)
{
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    task=nullptr;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* /*task*/)
{
    updateStatus();
}

void MainWindow::updateStatus()
{
    int completedCount = 0;
    foreach(auto const t,mTasks)  {
        if (t->isCompleted()) {
            completedCount++;
        }
    }
    int todoCount = mTasks.size();

    ui->statusLabel->setText(QString(tr("Status:  %1 / %2 completed"))
                             .arg(completedCount)
                             .arg(todoCount));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QFile file(FILENAME);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        foreach(auto const t , mTasks) {
            out << t->name() << "\n";
        }
    }

    QSettings settings("VSoftGR","ToDoList");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    QMainWindow::closeEvent(event);
}


void MainWindow::readSettings()
{
    QFile file(FILENAME);
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream in(&file);
        QString tmpTask;
        char buf[1024];
        while (!file.atEnd()) {
            tmpTask="";
            qint64 length = file.readLine(buf,sizeof(buf));
            if (length == -1) { break; }
            buf[length-1]='\0';
            tmpTask.append(buf);
            Task* task = new Task(tmpTask);
            connect(task, &Task::removed, this, &MainWindow::removeTask);
            connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
            mTasks.append(task);
            ui->tasksLayout->addWidget(task);
        }
    }

    QSettings settings("VSoftGR","ToDoList");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
}
